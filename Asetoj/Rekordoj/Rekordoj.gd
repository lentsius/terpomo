extends Node

var prenante : = false
	
var rekordoj : Dictionary = {}
	
signal rekordojn(vortaro)
signal sukceso
	
func konservi_rekordon(nomo : String, adreso : String, komento : String, rekordo : int) -> void:
	prenante = false
	
	var headers : PoolStringArray = ["Content-Type: application/json"]
	var URL : String = "https://terpomo-797c5.firebaseio.com/rekordoj.json"
	var to_send : Dictionary = {
		"nomo" : nomo,
		"retposxtadreso" : adreso,
		"komento" : komento,
		"rekordo" : rekordo
		}
	$HTTPRequest.request(URL, headers, false, HTTPClient.METHOD_POST, JSON.print(to_send))


func preni_rekordojn() -> void:
	prenante = true
	
	var headers : PoolStringArray = ["Content-Type: application/json"]
	var URL : String = "https://terpomo-797c5.firebaseio.com/rekordoj.json"
	$HTTPRequest.request(URL, headers, false, HTTPClient.METHOD_GET)


func _on_HTTPRequest_request_completed(result, response_code, headers, body):
#	print("rezulto: ", result)
	if result == 200 || result == 0:
		print("success!")
		emit_signal("sukceso")
	else:
		emit_signal("rekordojn", null)
		return
	
	if !prenante:
		return
		
	var body_json = body.get_string_from_utf8()
	var dict : Dictionary = parse_json(body_json)
	rekordoj = dict
	emit_signal("rekordojn", rekordoj)
