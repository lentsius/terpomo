extends HBoxContainer

func vortigxi(nombro : int, nomo : String, rekordo : int, komento : String) -> void:
	$Nombro.text = str(nombro)
	$Nomo.text = nomo
	$Rekordo.text = str(rekordo)
	$Komento.text = komento
	

func preni_rekordon() -> int:
	return int($Rekordo.text)

func nombro(nova : int) -> void:
	$Nombro.text = str(nova)
	
func preni_informojn() -> Array:
	var aro : Array = [$Nombro.text,$Nomo.text,$Rekordo.text,$Komento.text]
	return aro
