extends Node

var ludato : KinematicBody2D

var rapideco : = 100
var glateco : = 5

var movigxi : Vector2 = Vector2()
var glatmovigxi : Vector2 = Vector2()

func komenci(o) -> void:
	ludato = o
	
func kuri(delta) -> void:
	movigxi.x = (Globala.rapideco * rapideco) * Globala.direkto
	glatmovigxi = glatmovigxi.linear_interpolate(movigxi, glateco * delta)
	ludato.move_and_slide(glatmovigxi)
	
	kontroli_randon()
	
func kontroli_randon() -> void:
	if ludato.position.x >= 1300:
		ludato.position.x = -20
	elif ludato.position.x <= -20:
		ludato.position.x = 1300
