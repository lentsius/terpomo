extends Node

var ludato : KinematicBody2D

func eniri(o) -> void:
	ludato = o
	ludato.sxangxi_animacion("Kuro")
	ludato.kur_partikloj(true)
	Globala.rapideco = 6.0
	set_process_input(true)
	
func eliri() -> void:
	set_process_input(false)
	if ludato != null:
		ludato.kur_partikloj(false)
	
func gxisdatado(delta) -> String:
	if Input.is_action_just_pressed("ui_accept"):
		return "marsxi"
	return ""

func fizika_gxisdato(delta) -> String:
	return ""
