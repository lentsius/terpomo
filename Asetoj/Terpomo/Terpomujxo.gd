extends KinematicBody2D

var statoj : Dictionary

var stato
var antaua_stato

var direkto : String = ""

func _ready():
	kur_partikloj(false)
	statoj["marsxi"] = $Statoj/Marsxi
	statoj["kuri"] = $Statoj/Kuri
	statoj["atendi"] = $Statoj/Atendi
	$Statoj/Direktoj.komenci(self)
	$Statoj/Kurado.komenci(self)
	
	sxangxi_staton("marsxi")
	
	
func _physics_process(delta) -> void:
	$Statoj/Kurado.kuri(delta)
	var respondo = stato.fizika_gxisdato(delta)
	if respondo != "":
		sxangxi_staton(respondo)


func _process(delta) -> void:
	$Statoj/Direktoj.direktoj()
	var respondo = stato.gxisdatado(delta)
	if respondo != "":
		sxangxi_staton(respondo)
		
		
func sxangxi_staton(nova : String) -> void:
	if statoj[nova] == stato:
		return
	
	if stato != null:
		stato.eliri()
		antaua_stato = stato
	stato = statoj[nova]
	stato.eniri(self)

	
func sxangxi_animacion(nova : String, reen : bool = false) -> void:
	$AnimatedSprite.play(nova, reen)
	
	
func turnigxi(kien : String = "") -> void:
	match kien:
		"maldekstren":
			$AnimatedSprite.flip_h = true
			$Partikloj.scale.x = -1
			Globala.direkto = -1
		"dekstren":
			$AnimatedSprite.flip_h = false
			$Partikloj.scale.x = 1
			Globala.direkto = 1
	

func kur_partikloj(cxu : bool = false) -> void:
	for partiklo in $Partikloj.get_children():
		partiklo.emitting = cxu
