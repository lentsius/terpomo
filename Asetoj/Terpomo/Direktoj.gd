extends Node

var ludato : KinematicBody2D

func komenci(o) -> void:
	ludato = o

func direktoj() -> void:
	if Input.is_action_just_pressed("ui_left"):
		ludato.turnigxi("maldekstren")
	
	if Input.is_action_just_pressed("ui_right"):
		ludato.turnigxi("dekstren")
