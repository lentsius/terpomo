extends KinematicBody2D


var statoj : Dictionary = {}
var stato 
var antaua_stato

var unua_rapideco : int = 10

func _ready() -> void:
	set_collision_layer_bit(0, false)
	set_collision_layer_bit(randi() % 20, true)
	randomize()
	specigxi()
	statoj["falonte"] = $Statoj/Falonte
	statoj["falante"] = $Statoj/Falante
	statoj["falinte"] = $Statoj/Falinte
	statoj["trafinte"] = $Statoj/Trafinte
	
	sxangxi_staton("falonte")


func _physics_process(delta):
	var sxangxo = stato.fizika_gxisdato(delta)
	if sxangxo != "":
		sxangxi_staton(sxangxo)
		
func _process(delta):
	var sxangxo = stato.gxisdatado(delta)
	if sxangxo != "":
		sxangxi_staton(sxangxo)


func sxangxi_staton(nova : String) -> void:
	if statoj[nova] == stato:
		return
	
	if stato != null:
		stato.eliri()
		antaua_stato = stato
	stato = statoj[nova]
	stato.eniri(self)


func malaktivigi() -> void:
	$CollisionShape2D.queue_free()
	$Tvino.interpolate_property(self, "modulate:a", 0.3, 0.0, 1.0, Tween.TRANS_QUAD, Tween.EASE_OUT, 0.0)
	$Tvino.start()
	yield(get_tree().create_timer(1), "timeout")
	queue_free()
	
func specigxi() -> void:
	var sxanco = rand_range(0, 1)
	
	if sxanco > 0 and sxanco < 0.5:
		speco("argxenta")
	elif sxanco > 0.5 and sxanco < 0.75:
		speco("kupra")
	else:
		speco("ora")


func speco(nova : String) -> void:
	match nova:
		"argxenta":
			$Specoj/Argxenta.visible = true
		"kupra":
			$Specoj/Kupra.visible = true
			unua_rapideco = 100
		"ora":
			$Specoj/Ora.visible = true
			unua_rapideco = 250
		_:
			print("nekonata speco")

func finigxi() -> void:
	queue_free()
