extends Node

var pinglo : KinematicBody2D

var movigxo : Vector2 = Vector2()



func eniri(o) -> void:
	pinglo = o
	movigxo.y = pinglo.unua_rapideco * Globala.pinglrapideco
	movigxo = movigxo.rotated(deg2rad(pinglo.rotation_degrees))
	
	
func eliri() -> void:
	Globala.pinglo_scenejen -= 1
	
	
func gxisdatado(delta) -> String:
	if pinglo.position.x <= 0:
		pinglo.position.x = 1279
	elif pinglo.position.x >= 1280:
		pinglo.position.x = 1
		
	if pinglo.position.y > 840:
		return "falinte"
		
	movigxo *= 1.005
	return ""


func fizika_gxisdato(delta) -> String:
	var kolizio : KinematicCollision2D = pinglo.move_and_collide(movigxo * delta, false)
	#kontroli kolizion, se estis kun ludanto TRAFU se ne FALU
	if kolizio:
		if kolizio.collider.name == "Tero":
			return "falinte"
		elif kolizio.collider.name == "Terpomujxo":
			return "trafinte"
	return ""
