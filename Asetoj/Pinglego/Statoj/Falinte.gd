extends Node

var pinglo : KinematicBody2D
var direkto : Vector2 = Vector2()

var distanco : float = 35

func eniri(o) -> void:
	pinglo = o
	direkto = Vector2(0,1).rotated(deg2rad(pinglo.rotation_degrees))
	direkto *= distanco
	$Tvino.interpolate_property(pinglo, "position", pinglo.position, pinglo.position + direkto, 1.0, Tween.TRANS_EXPO, Tween.EASE_OUT, 0.0)
	$Tvino.interpolate_property(pinglo, "modulate:a", 1.0, 0.3, 1.0, Tween.TRANS_EXPO, Tween.EASE_OUT, 0.0)
	$Tvino.start()
	
	pinglo.malaktivigi()
	
	Globala.doni_rekordon(1)
	
	## komentite cxar la ludmotoro cimigxis
#	yield(get_tree().create_timer(1), "timeout")
#
#	$Tvino.interpolate_property(pinglo, "modulate:a", 0.3, 0.0, 5.0, Tween.TRANS_EXPO, Tween.EASE_OUT, 0.0)
#	$Tvino.start()
#
#	yield(get_tree().create_timer(5), "timeout")
	
#	pinglo.finigxi()
	
	
	
func eliri() -> void:
	pass
	
	
func gxisdatado(delta) -> String:
	return ""


func fizika_gxisdato(delta) -> String:
	return ""
