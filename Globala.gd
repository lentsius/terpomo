extends Node

var rapideco : float = 1.0
var direkto : int = 1

var pinglrapideco : float = 1.0

var rekordo : int = 0

var UI : Control
var kreilo : Node2D

var pinglo_scenejen : int = 0

func doni_rekordon(nova) -> void:
	rekordo += nova
	UI.rekordo(rekordo)
	
	malplifaciligi()
	
func malplifaciligi() -> void:
	if rekordo > 25 and rekordo < 50:
		kreilo.ofteco = 0.3
	elif rekordo > 50 and rekordo < 100:
		kreilo.ofteco = 0.2
	elif rekordo > 150:
		kreilo.ofteco = 0.1

func fino() -> void:
	UI.fino()
	get_tree().paused = true

func restartigxi() -> void:
	rekordo = 0
