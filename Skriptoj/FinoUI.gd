extends Control

onready var nomkampo = $VBoxContainer/CenterContainer/HBoxContainer/u_nomo
onready var rekordkampo = $VBoxContainer/CenterContainer/HBoxContainer/u_rekordo
onready var komentkampo = $VBoxContainer/CenterContainer/HBoxContainer/u_komento
onready var adreskampo = $VBoxContainer/CenterContainer/HBoxContainer/u_adreso


func _ready() -> void:
	Rekordoj.connect("sukceso", self, "sukceso")
	$Mesagxo.modulate.a = 0
	$Mesagxo.visible = true


func _on_Nuligi_pressed():
	cxefa_menuen()
	
	
func _on_Forsendi_pressed():
	var enorde = kontroli_kampojn()
	if enorde:
		Rekordoj.konservi_rekordon(nomkampo.text, adreskampo.text, komentkampo.text, int(rekordkampo.text))
	else:
		mesagxo("Kontrolu la nomkampon")
		
		
func _on_LudiDenove_pressed():
	get_tree().paused = false
	Globala.restartigxi()
	get_tree().reload_current_scene()
	
	
func kontroli_kampojn() -> bool:
	var enorde : bool = true
	
	if nomkampo.text == "":
		enorde = false
	
	return enorde
	
	
func mesagxo(teskto : String = "") -> void:
	$Tvino.interpolate_property($Mesagxo, "modulate:a", $Mesagxo.modulate.a, 1, 1, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
	$Tvino.interpolate_property($Mesagxo, "modulate:a", $Mesagxo.modulate.a, 0, 1, Tween.TRANS_EXPO, Tween.EASE_OUT, 1)
	$Tvino.start()
	
	
func eniri() -> void:
	rekordkampo.text = str(Globala.rekordo)

func sukceso() -> void:
	cxefa_menuen()
	print("sukcese enskribis la rekordon")
	
func cxefa_menuen() -> void:
	Globala.restartigxi()
	get_tree().paused = false
	get_tree().change_scene("res://Scenoj/CxefaMenuo.tscn")
	
