extends Node

signal idle_mouse

func _input(event):
	if event is InputEventMouseMotion:
		$Timer.start()


func _on_Timer_timeout():
	emit_signal("idle_mouse")
