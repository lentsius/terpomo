extends ColorRect

var rekordoj : Dictionary = {}
var rekord_valoroj : Array = []
onready var rekord_sxablono = preload("res://Asetoj/Rekordoj/Sxablono.tscn")

var konservitaj_rekordoj : Array

func _ready() -> void:
	sxaltigxi()
	mouse_filter = Control.MOUSE_FILTER_IGNORE
	Rekordoj.connect("rekordojn", self, "ricevas_rekordojn")
	modulate.a = 0
#	testplenigi()
	
func testplenigi() -> void:
	Rekordoj.konservi_rekordon("John", "retposxtadreso", "mi amas pomojn", 549841)
	Rekordoj.konservi_rekordon("Black", "retposxtadreso", "mi amas TERpomojn", 23)
	Rekordoj.konservi_rekordon("Alice", "retposxtadreso", "Forirunta", 500)
	Rekordoj.konservi_rekordon("Monoa", "retposxtadreso", "Verdire", 7500)
	Rekordoj.konservi_rekordon("Troa", "retposxtadreso", "CXUUU", 122523)

func sxaltigxi() -> void:
	Rekordoj.preni_rekordojn()

func ricevas_rekordojn(vortaro) -> void:
	if vortaro == null:
		print(vortaro)
		print("eraro")
		$Eraro.visible = 1
		return
		
	$VBoxContainer/Rollumujo/Rekordoj.visible = 1
	rekordoj.clear()
	rekordoj = vortaro
	plenigxi()
	

func plenigxi() -> void:
	for malnova in $VBoxContainer/Rollumujo/Rekordoj.get_children():
		if malnova.name != "Informoj":
			malnova.queue_free()
			
	yield(get_tree(), "idle_frame")
	var sxlosiloj = rekordoj.keys()
	var nombro = 1
	
	rekord_valoroj.clear()
	
	for rekordo in sxlosiloj:
		var nova_rekordo = rekord_sxablono.instance()
		var nomo = rekordoj[rekordo].nomo
		var komento = rekordoj[rekordo].komento
		var rekord_numero = rekordoj[rekordo].rekordo
		rekord_valoroj.append(int(rekord_numero))
		
		nova_rekordo.vortigxi(nombro, nomo, rekord_numero, komento)
		
		$VBoxContainer/Rollumujo/Rekordoj.add_child(nova_rekordo)
		nombro += 1

	
	arangxi()
		
func arangxi() -> void:
	rekord_valoroj.sort()
	var list_nombro : Array = []
	
	var rekordoj = $VBoxContainer/Rollumujo/Rekordoj.get_children()
	
	for rekordo in rekordoj:
		var kie = rekord_valoroj.find(rekordo.preni_rekordon())
		list_nombro.append(kie)

	
	while rekord_valoroj.size() != 0:
		for i in rekordoj:
			if rekord_valoroj.size() == 0 :
				break
				
			if i.preni_rekordon() == rekord_valoroj[0]:
				$VBoxContainer/Rollumujo/Rekordoj.move_child(i, 0)
				if rekord_valoroj.size() > 0:
					rekord_valoroj.pop_front()
	
	var loko : int = 0
	var fin_listo : Dictionary = {}
	
	loko = 1
	
	konservitaj_rekordoj.clear()
	
	# nombrigu la rekordojn lau vice, komencante de 1
	# kaj aldonu cxiujn al la listo de finaj rekordoj
	for i in $VBoxContainer/Rollumujo/Rekordoj.get_children():
		i.nombro(loko)
		loko += 1
		var info : Array = i.preni_informojn()
		fin_listo = {"nombro" : info[0], "nomo" : info[1], "rekordo" : info[2], "komento" : info[3]}
		konservitaj_rekordoj.append(fin_listo)
			

func eniri() -> void:
	visible = 1
	$Tvino.interpolate_property(self, "modulate:a", 0, 1, 1.0, Tween.TRANS_EXPO, Tween.EASE_OUT, 0.0)
	$Tvino.start()
	$Subo/Reen.mouse_filter = Control.MOUSE_FILTER_STOP
	$Subo/Kontroli.mouse_filter = Control.MOUSE_FILTER_STOP
	
	
func eliri() -> void:
	$Tvino.interpolate_property(self, "modulate:a", 1, 0, 1.0, Tween.TRANS_EXPO, Tween.EASE_OUT, 0.0)
	$Tvino.start()
	$Subo/Reen.mouse_filter = Control.MOUSE_FILTER_IGNORE
	$Subo/Kontroli.mouse_filter = Control.MOUSE_FILTER_IGNORE


func _on_Reen_pressed():
	eliri()


func _on_Kontroli_pressed():
	sxaltigxi()
