extends Control

var centro : bool = false
var sep : bool = false

func _ready() -> void:
	randomize()
	
	
func _on_MusoSuperTerpomo_mouse_entered() -> void:
	$Terpomo.play("Kuro")
	
	
func _on_MusoSuperTerpomo_mouse_exited() -> void:
	$Terpomo.play("Promeno")
	
	
func _on_MusoSuperTerpomo_gui_input(event) -> void:
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		$Terpomo.flip_h = !$Terpomo.flip_h


func _on_SEP2019_mouse_entered() -> void:
	if sep:
		return
	var kadro : ColorRect = $BlankKadro
	$BlankKadro/SEP2019.mouse_filter = Control.MOUSE_FILTER_IGNORE
	kadro.mouse_filter = Control.MOUSE_FILTER_IGNORE
	ludi_klakon()
	$Tween.stop_all()
	$Tween.interpolate_property(kadro, 'rect_position:x', 1280, 933, 1.0, Tween.TRANS_EXPO, Tween.EASE_OUT, 0.0)
	$Tween.interpolate_property($BlankKadro/Ujo/Label, 'percent_visible', 0.0, 1.0, 2.5, Tween.TRANS_EXPO, Tween.EASE_OUT, 0.0)
	$Tween.interpolate_property($BlankKadro/Ujo/Label, 'modulate:a', 0.0, 1.0, 1.0, Tween.TRANS_EXPO, Tween.EASE_OUT, 0.0)
	$Tween.interpolate_property($BlankKadro/SEP2019, 'modulate:a', 1.0, 0.5, 1.0, Tween.TRANS_EXPO, Tween.EASE_OUT, 0.0)
	$Tween.start()
	
	sep = true
	
	yield($Tween, "tween_completed")
	kadro.mouse_filter = Control.MOUSE_FILTER_STOP


func _on_BlankKadro_mouse_exited() -> void:
	if sep == false:
		return
		
	var kadro : ColorRect = $BlankKadro
	$BlankKadro/SEP2019.mouse_filter = Control.MOUSE_FILTER_IGNORE
	kadro.mouse_filter = Control.MOUSE_FILTER_IGNORE
	ludi_klakon()
	
	$Tween.stop_all()
	$Tween.interpolate_property(kadro, 'rect_position:x', 933, 1280, 1.0, Tween.TRANS_EXPO, Tween.EASE_OUT, 0.0)
	$Tween.interpolate_property($BlankKadro/Ujo/Label, 'percent_visible', 1.0, 0.0, 1.0, Tween.TRANS_EXPO, Tween.EASE_OUT, 0.0)
	$Tween.interpolate_property($BlankKadro/Ujo/Label, 'modulate:a', 1.0, 0.0, 1.0, Tween.TRANS_EXPO, Tween.EASE_OUT, 0.0)
	$Tween.interpolate_property($BlankKadro/SEP2019, 'modulate:a', 0.5, 1.0, 1.0, Tween.TRANS_EXPO, Tween.EASE_OUT, 0.0)
	$Tween.start()
	
	sep = false
	
	yield($Tween, "tween_completed")
	$BlankKadro/SEP2019.mouse_filter = Control.MOUSE_FILTER_STOP
	
	
func ludi_klakon() -> void:
	$Klako.play()


func _on_Rekordoj_pressed():
	$Rekordoj.eniri()


func _on_Aligxi_mouse_entered():
	$Terpomo.play("Kuro")


func _on_Aligxi_mouse_exited():
	$Terpomo.play("Promeno")


func _on_Aligxi_pressed():
	OS.shell_open("https://ses.ikso.net/2019/en/aligxilo.php")


func _on_Lentsius_pressed():
	OS.shell_open("https://lentsius-bark.itch.io")


func _on_SEP_ret_pressed():
	OS.shell_open("http://sep.ikso.net")


func _on_SES_pressed():
	OS.shell_open("https://ses.ikso.net")
