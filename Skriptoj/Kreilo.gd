extends Node2D

onready var pinglego = preload("res://Asetoj/Pinglego/Pinglego.tscn")
var ofteco : float = 0.4
const margin : int = 25

func _ready():
	$Timer.connect("timeout", self, "tempilo_signalo")
	Globala.kreilo = self
	
	
func tempilo_signalo() -> void:
	$Timer.wait_time = ofteco
	krei_novan_pinglegon()
	$Tween.interpolate_property($Linio, "modulate:a", 1.0, 0.3, 1.0, Tween.TRANS_EXPO, Tween.EASE_OUT, 0.0)
	$Tween.interpolate_property($Linio, "width", 3.0, 0.5, 1.0, Tween.TRANS_EXPO, Tween.EASE_OUT, 0.0)
	$Tween.start()


func krei_novan_pinglegon() -> void:
	if Globala.pinglo_scenejen > 100:
		return
	var nova = pinglego.instance()
	nova.position.x = rand_range(0 + margin, 1280 - margin)
	add_child(nova)
	Globala.pinglo_scenejen += 1
	
